/**
 * Created by macbook on 3/29/17.
 */

const {restClient, gtxClient} = require('postchain-client');


const id = process.argv[2];
const authority = process.argv[3];
const url = process.argv[4];
const blockchainRID = Buffer.from(process.argv[5], 'hex');

const myRestClient = restClient.createRestClient(url);
const myGtxClient = gtxClient.createClient(myRestClient, blockchainRID, ["certificate"]);

myRestClient.query({type:'get_certificates', id, authority},
    function (err, res) {
        console.log(err);
        console.log(JSON.stringify(res))
    });
