const {restClient, gtxClient} = require('postchain-client');

const id = process.argv[2];
const name = process.argv[3];
const pubkey = Buffer.from(process.argv[4], 'hex');
const expires = Math.floor(Date.now()) + 60*60*24*365;
const reason = Buffer.from(process.argv[5]);
const auPubKey = Buffer.from(process.argv[6], 'hex');
const auPrivKey = Buffer.from(process.argv[7], 'hex');
const url = process.argv[8];
const blockchainRID = Buffer.from(process.argv[9], 'hex');

const myRestClient = restClient.createRestClient(url);
const myGtxClient = gtxClient.createClient(myRestClient, blockchainRID, ["certificate"]);
const rq = myGtxClient.newRequest([auPubKey]);
rq.certificate(id, name, pubkey, expires, auPubKey, reason);
rq.sign(auPrivKey);
rq.send( (err, res) => {
    console.log(err, res);
})








