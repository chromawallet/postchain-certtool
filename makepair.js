var crypto = require('crypto');
var secp256k1 = require('secp256k1');

let privKey;
do {
    privKey = crypto.randomBytes(32);
} while (!secp256k1.privateKeyVerify(privKey));
const pubKey = secp256k1.publicKeyCreate(privKey);


console.log("Pub ", pubKey.toString('hex'));
console.log("Priv ", privKey.toString('hex'));
